///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello1.cpp
/// @version 1.0 - Initial version
///
/// Compile:
///   make
///
/// Example:
///   $ ./hello1
///   Hello World
///
/// @author  Paulo Baldovi <pbaldovi@hawaii.edu>
/// @date    01 Mar 2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main() {
   cout << "Hello World" << endl;
   
   return 0;
}
