///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello3.cpp
/// @version 1.0 - Initial version
///
/// Compile:
///   make
///
/// Example:
///   $ ./hello3
///   Meow
///
/// @author  Paulo Baldovi <pbaldovi@hawaii.edu>
/// @date    01 Mar 2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

class Cat {
   public:
      void sayHello() {
         cout << "Meow" << endl;
      }
};

int main() {
   Cat myCat;
   
   myCat.sayHello();

   return 0;
}
